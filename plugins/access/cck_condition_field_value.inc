<?php

/**
 * @file
 * Plugin to provide access control based upon CCK field value.
 */

$plugin = array(
  'title' => t("Node: field value"),
  'description' => t('Control access by CCK field value.'),
  'callback' => 'cck_conditions_compare',
  'settings form' => 'cck_condition_settings',
  'summary' => 'cck_condition_summary',
  'required context' => new ctools_context_required(t('Node'), 'node'),
);

/**
 * Settings form for the 'by CCK field value' access plugin.
 */
function cck_condition_settings(&$form, &$form_state, $conf) {
  $supported_field_types = implode(', ', array_keys(cck_condition_storage_keys()));

  $form['settings']['cckfield'] = array(
    '#type' => 'select',
    '#title' => t('CCK field'),
    '#default_value' => $conf['cckfield'],
    '#options' => cck_condition_fields(),
    '#description' => t('The CCK field that you want to match. The following field types are supported '.$supported_field_types),
  );
  $form['settings']['requiredvalue'] = array(
    '#type' => 'textfield',
     '#default_value' => $conf['requiredvalue'],
    '#title' => t('The value to compare with'),
    '#description' => t('The CCK field value that you want to match with.'),
  );
}

/**
 * Compare CCK field value with the required value.
 */
function cck_conditions_compare($conf, $context) {

  //Return false if there is no value to use
  if (empty($context) || empty($context->data) || !isset($context->data->{$conf['cckfield']})) {
    return FALSE;
  }

  //Get all info about the CCK field
  $cckfield_info = content_fields($conf['cckfield']);
  
  //Get the field type name for the CCK field
  $field_type_name = $cckfield_info['type'];

  //Get the value name for the CCK field type
  $storage_key = cck_condition_storage_keys($field_type_name);

  //Compare if the chosen CCK fields value matches the value that has been supplied
  if ($context->data->{$conf['cckfield']}[0][$storage_key] == $conf['requiredvalue']) {
    return TRUE;
  }
}

/**
 * Provide a summary description based upon the chosen CCK field and supplied CCK field value
 */
function cck_condition_summary($conf, $context) {
  return t('@cckfield has value "@requiredvalue"', array('@cckfield' => $conf['cckfield'],'@requiredvalue' => $conf['requiredvalue']));
}
